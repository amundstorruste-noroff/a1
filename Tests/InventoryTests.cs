﻿using System;
using Xunit;
using DunkDunkers_RPG;
using DunkDunkers_RPG.CustomExceptions;
using DunkDunkers_RPG.DataTypes;
using System.IO;
using DunkDunkers_RPG.Character;

namespace Tests
{
    public class InventoryTests
    {
        [Fact]
        public void AddWeaponToInventory_tooHighLvlItem_shouldThrowInvalidWeaponException()
        {
            // Arrange
            Weapon level2Axe = new()
            {
                ItemName = "Level 2 axe",
                ItemLevel = 2,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            var level1Hero = new Warrior("Brutus");
            string expected = "Too low level.";


            // Act and assert
            var exception = Assert.Throws<InvalidWeaponException>(() => level1Hero.EquipWeapon(level2Axe));
            Assert.Equal(expected, exception.Message);
        }

        [Fact]
        public void AddArmorToInventory_tooHighLvlItem_shouldThrowInvalidArmorException()
        {
            // Arrange
            Armor lvl2PlateBody = new()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 2,
                ItemSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_PLATE,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };
            var level1Hero = new Warrior("Brutus");
            string expected = "Too low level.";


            // Act and assert
            var exception = Assert.Throws<InvalidArmorException>(() => level1Hero.EquipArmor(lvl2PlateBody));
            Assert.Equal(expected, exception.Message);

        }

        [Fact]
        public void AddWeaponToInventory_WarriorEquipsBow_shouldThrowInvalidWeaponException()
        {
            // Arrange
            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_BOW,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };

            var level1Hero = new Warrior("Brutus");
            string expected = "Invalid weapon type.";


            // Act and assert
            var exception = Assert.Throws<InvalidWeaponException>(() => level1Hero.EquipWeapon(testBow));
            Assert.Equal(expected, exception.Message);
        }

        [Fact]
        public void AddArmorToInventory_WarriorEquipsCloth_shouldThrowInvalidArmorException()
        {
            // Arrange
            Armor testClothHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_CLOTH,
                Attributes = new PrimaryAttributes() { Intelligence = 5 }
            };
            var level1Hero = new Warrior("Brutus");
            string expected = "Invalid armor type.";


            // Act and assert
            var exception = Assert.Throws<InvalidArmorException>(() => level1Hero.EquipArmor(testClothHead));
            Assert.Equal(exception.Message, expected);

        }

        [Fact]
        public void AddWeaponToInventory_WarriorEquipsAxe_shouldDisplaySucessMessage()
        {
            // Arrange
            Weapon testAxe = new()
            {
                ItemName = "Level 2 axe",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            var level1Hero = new Warrior("Brutus");
            string expected = "New weapon equipped!";


            // Act
            // Redirect output to a stringwriter
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);
            
            // Add weapon to inventory, expecting success message
            level1Hero.EquipWeapon(testAxe);
            
            // Assert
            Assert.Equal(expected, stringWriter.ToString().Trim());


        }

        [Fact]
        public void AddArmorToInventory_WarriorEquipsPlate_shouldDisplaySucessMessage()
        {
            // Arrange
            Armor testPlateBody = new()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_PLATE,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };

            var level1Hero = new Warrior("Brutus");
            string expected = "New armor equipped!";


            // Act
            // Redirect output to a stringwriter
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            // Add weapon to inventory, expecting success message
            level1Hero.EquipArmor(testPlateBody);

            // Assert
            Assert.Equal(expected, stringWriter.ToString().Trim());


        }
    }
}
