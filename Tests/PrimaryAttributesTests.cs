using DunkDunkers_RPG;
using DunkDunkers_RPG.DataTypes;
using System;
using Xunit;

namespace Tests
{
    public class PrimaryAttributesTests
    {
        [Fact]
        public void Operator_overload_should_add_respective_attributes()
        {
            // Arrange
            PrimaryAttributes actual = new()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 1
            };
            PrimaryAttributes expected = new()
            {
                Strength = 2,
                Dexterity = 2,
                Intelligence = 2
            };

            // Act
            actual += actual;

            // Assert
            Assert.Equal(actual, expected);   
        }
    }
}
