using DunkDunkers_RPG;
using DunkDunkers_RPG.Character;
using DunkDunkers_RPG.DataTypes;
using System;
using Xunit;

namespace Tests
{
    public class HeroTests
    {
        [Fact]
        public void Hero_Warrior_is_level_1_when_created()
        {
            // Arrange
            int expected_level = 1;

            // Act
            var AmundTheBrute = new Warrior("Brutus");
            int actual_level = AmundTheBrute.Level;

            // Assert
            Assert.Equal(expected_level, actual_level);   
        }

        [Fact]
        public void Hero_Warrior_is_level_2_after_leveling_up()
        {
            // Arrange
            int expected_level = 2;

            // Act
            var AmundTheBrute = new Warrior("Brutus");
            AmundTheBrute.LevelUp();
            int actual_level = AmundTheBrute.Level;

            // Assert
            Assert.Equal(expected_level, actual_level);
        }

        [Fact]
        public void Hero_Mage_has_correct_level_one_stats()
        {
            // Arrange
            PrimaryAttributes expectedStats = new()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8
            };

            // Act
            var testHero = new Mage("Magimannen");

            // Assert
            Assert.Equal(testHero.TotalPrimaryAttributes, expectedStats);
        }

        [Fact]
        public void Hero_Mage_has_correct_level_two_stats()
        {
            // Arrange
            PrimaryAttributes expectedStats = new()
            {
                Strength = 2,
                Dexterity = 2,
                Intelligence = 13
            };

            // Act
            var testHero = new Mage("Magimannen");
            testHero.LevelUp();

            // Assert
            Assert.Equal(testHero.TotalPrimaryAttributes, expectedStats);
        }

        [Fact]
        public void Hero_Ranger_has_correct_level_one_stats()
        {
            // Arrange
            PrimaryAttributes expectedStats = new()
            {
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1
            };

            // Act
            var testHero = new Ranger("xx_legolas_xx");

            // Assert
            Assert.Equal(testHero.TotalPrimaryAttributes, expectedStats);
        }

        [Fact]
        public void Hero_Ranger_has_correct_level_two_stats()
        {
            // Arrange
            PrimaryAttributes expectedStats = new()
            {
                Strength = 2,
                Dexterity = 12,
                Intelligence = 2
            };

            // Act
            var testHero = new Ranger("xx_legolas_xx");
            testHero.LevelUp();


            // Assert
            Assert.Equal(testHero.TotalPrimaryAttributes, expectedStats);
        }

        [Fact]
        public void Hero_Rogue_has_correct_level_one_stats()
        {
            // Arrange
            PrimaryAttributes expectedStats = new()
            {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1
            };

            // Act
            var testHero = new Rogue("Mr. Snek");

            // Assert
            Assert.Equal(testHero.TotalPrimaryAttributes, expectedStats);
        }

        [Fact]
        public void Hero_Rogue_has_correct_level_two_stats()
        {
            // Arrange
            PrimaryAttributes expectedStats = new()
            {
                Strength = 3,
                Dexterity = 10,
                Intelligence = 2
            };

            // Act
            var testHero = new Rogue("Mr. Snek");
            testHero.LevelUp();

            // Assert
            Assert.Equal(testHero.TotalPrimaryAttributes, expectedStats);
        }

        [Fact]
        public void Hero_Warrior_has_correct_level_one_stats()
        {
            // Arrange
            PrimaryAttributes expectedStats = new()
            {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1
            };

            // Act
            var testHero = new Warrior("Brutus");

            // Assert
            Assert.Equal(testHero.TotalPrimaryAttributes, expectedStats);
        }

        [Fact]
        public void Hero_Warrior_has_correct_level_two_stats()
        {
            // Arrange
            PrimaryAttributes expectedStats = new()
            {
                Strength = 8,
                Dexterity = 4,
                Intelligence = 2
            };

            // Act
            var testHero = new Warrior("Brutus");
            testHero.LevelUp();

            // Assert
            Assert.Equal(testHero.TotalPrimaryAttributes, expectedStats);
        }
    }
}
