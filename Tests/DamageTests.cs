﻿using DunkDunkers_RPG;
using DunkDunkers_RPG.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class DamageTests
    {
        [Fact]
        public void CalculateDamage_withoutWeaponAtLvl1_shouldDealDamage()
        {
            // Arrange
            var AmundTheBrute = new Warrior("Brutus");
            double expected = 1.05;

            // Act
            double actual = AmundTheBrute.CalculateDamage();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDamage_witWeaponAtLvl1_shouldDealDamage()
        {
            // Arrange
            var AmundTheBrute = new Warrior("Brutus");
            Weapon level1Axe = new()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            AmundTheBrute.EquipWeapon(level1Axe);
            double expected = (7*1.1)*(1+0.05);

            // Act
            double actual = AmundTheBrute.CalculateDamage();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDamage_witWeaponAndArmorAtLvl1_shouldDealDamage()
        {
            // Arrange
            var AmundTheBrute = new Warrior("Brutus");
            Weapon level1Axe = new()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor lvl1PlateBody = new()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_PLATE,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };
            AmundTheBrute.EquipWeapon(level1Axe);
            AmundTheBrute.EquipArmor(lvl1PlateBody);
            double expected = (7 * 1.1) * (1 + 0.06);

            // Act
            double actual = AmundTheBrute.CalculateDamage();

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
