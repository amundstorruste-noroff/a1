﻿using System;
using System.Collections.Generic;

using DunkDunkers_RPG.CustomExceptions;
using DunkDunkers_RPG.Interfaces;
using DunkDunkers_RPG.Character;

namespace DunkDunkers_RPG
{
    /// <summary>
    /// Inventory that contains Weapon and Armor items that heroes can use to deal damage.
    /// </summary>
    public class Inventory
    {
        public Dictionary<Slot, Item> equipment = new();

        public Weapon GetWeapon()
        {
            if (equipment.ContainsKey(Slot.SLOT_WEAPON) == false) return null;
            return (Weapon)equipment[Slot.SLOT_WEAPON];
        }

        public List<Armor> GetArmorList()
        {
            List<Armor> armorList = new List<Armor>();
            foreach (KeyValuePair<Slot, Item> entry in equipment) if (entry.Value is Armor armor) armorList.Add(armor);
            return armorList;
        }

        /// <summary>
        /// Add or replace weapon in equipment dictionary. Assumes all eligibility and level requirements are fulfilled.
        /// </summary>
        /// <param name="weapon"></param>
        public void EquipWeapon(Weapon weapon)
        {
            equipment[weapon.ItemSlot] = weapon;
            Console.WriteLine("New weapon equipped!");
        }

        /// <summary>
        /// Add or replace armor in equipment dictionary. Assumes all eligibility and level requirements are fulfilled.
        /// </summary>
        /// <param name="armor"></param>
        public void EquipArmor(Armor armor)
        {
            equipment[armor.ItemSlot] = armor;
            Console.WriteLine("New armor equipped!");
        }

        
    }
}
