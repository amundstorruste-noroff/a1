﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DunkDunkers_RPG
{
    public enum Slot { SLOT_HEAD, SLOT_BODY, SLOT_LEGS, SLOT_WEAPON };

    /// <summary>
    /// Items are either weapons or armor and can be put in an Inventory item slot. They are used by Combat Heroes to increase damage.
    /// </summary>
    public abstract class Item
    {
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public Slot ItemSlot { get; set; }
    }
}
