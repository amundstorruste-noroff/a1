﻿namespace DunkDunkers_RPG
{
    public enum WeaponType{ WEAPON_AXE, WEAPON_BOW, WEAPON_DAGGER, WEAPON_HAMMER, WEAPON_STAFF, WEAPON_SWORD, WEAPON_WAND};
    public struct WeaponAttributes 
    { 
        public double Damage { get; set; }
        public double AttackSpeed { get; set; }
    };

    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set;}

        public WeaponAttributes WeaponAttributes { get; set; }

        public double GetDPS()
        {
            return WeaponAttributes.Damage * WeaponAttributes.AttackSpeed;
        }

    }
}
