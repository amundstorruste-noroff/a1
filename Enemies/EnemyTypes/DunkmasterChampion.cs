﻿using System;
using System.Collections.Generic;
namespace DunkDunkers_RPG.Enemies.EnemyTypes
{
    /// <summary>
    /// Miniboss of in the DunkDunkers RPG. Gets really angry.
    /// </summary>
    public class DunkmasterChampion : Enemy
    {
        public DunkmasterChampion()
        {
            Name = "Dunkermaster Champion";
            Health = 100;
            AsciiArt = @"
                                        ,-.
                   ___,---.__          /'|`\          __,---,___
                ,-'    \`    `-.____,-'  |  `-.____,-'    //    `-.
              ,'        |           ~'\     /`~           |        `.
             /      ___//              `. ,'          ,  , \___      \
            |    ,-'   `-.__   _         |        ,    __,-'   `-.    |
            |   /          /\_  `   .    |    ,      _/\          \   |
            \  |           \ \`-.___ \   |   / ___,-'/ /           |  /
             \  \           | `._   `\\  |  //'   _,' |           /  /
              `-.\         /'  _ `---'' , . ``---' _  `\         /,-'
                 ``       /     \    ,='/ \`=.    /     \       ''
                         |__   /|\_,--.,-.--,--._/|\   __|
                         /  `./  \\`\ |  |  | /,//' \,'  \
            eViL        /   /     ||--+--|--+-/-|     \   \
                       |   |     /'\_\_\ | /_/_/`\     |   |
                        \   \__, \_     `~'     _/ .__/   /
                         `-._,-'   `-._______,-'   `-._,-'
            ";
        }

        public override void Attack()
        {
            Console.WriteLine("The Dunkermaster Champion smashes its shield in anger.");
        }
    }
}
