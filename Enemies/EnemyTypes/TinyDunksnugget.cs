﻿using System;

namespace DunkDunkers_RPG.Enemies.EnemyTypes
{
    /// <summary>
    /// A cute DunkDunker that flees if it has the possibility. Please don't hurt it!
    /// </summary>
    internal class TinyDunksnugget : Enemy
    {
        public TinyDunksnugget()
        {
            Name = "Tiny Dunksnugget";
            Health = 10;
            AsciiArt = @"
                  __ __
               .-',,^,,'.
              / \(0)(0)/ \
              )/( ,__,)\(
              `  >-`~(   ' 
            _N\ |(`\ |___
            \' |/ \ \/_-,)
             '.(  \`\_<
                \ _\|
                 | |_\_
            snd  \_,_>-'
";
        }

        public override void Attack()
        {
            Health = 0;
            Console.WriteLine("The Tiny Dunksnugget flicks it tail, whimpers, and flees into a crack in the wall.");
        }

    }
}
