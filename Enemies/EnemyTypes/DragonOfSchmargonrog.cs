﻿using System;

namespace DunkDunkers_RPG.Enemies.EnemyTypes
{
    /// <summary>
    /// The ultimate boss of the DunkDunkers RPG. Better bring a Greater Potion of Fire Protection!
    /// </summary>
    public class DragonOfSchmargonrog : Enemy
    {
        public DragonOfSchmargonrog()
        {
            Name = "The Dragon of Schmargonrog";
            Health = 1000;
            AsciiArt = @"
                            ^    ^
                           / \  //\
             |\___/|      /   \//  .\
             /O  O  \__  /    //  | \ \
            /     /  \/_/    //   |  \  \
            @___@'    \/_   //    |   \   \ 
               |       \/_ //     |    \    \ 
               |        \///      |     \     \ 
              _|_ /   )  //       |      \     _\
             '/,_ _ _/  ( ; -.    |    _ _\.-~        .-~~~^-.
             ,-{        _      `-.|.-~-.           .~         `.
              '/\      /                 ~-. _ .-~      .-~^-.  \
                 `.   {            }                   /      \  \
               .----~-.\        \-'                 .~         \  `. \^-.
              ///.----..>    c   \             _ -~             `.  ^-`   ^-_
                ///-._ _ _ _ _ _ _}^ - - - - ~                     ~--,   .-~
                                                                      /.-'
            ";
        }

        public override void Attack()
        {
            Console.WriteLine("The massive Dragon of Schmargonrog draws a deep breath and then spews out burning flames. " +
                "The heat is so intense that the rock walls are melting around you. Lucky thing you are immune to fire damage!");
        }
    }
}
