﻿namespace DunkDunkers_RPG.Enemies
{
    /// <summary>
    /// Enemy class that can be fought by a CombatHero.
    /// </summary>
    public abstract class Enemy
    {
        public string Name { get; set; }
        public int Health { get; set; }
        public string AsciiArt { get; init; }
        public abstract void Attack();
    }
}
