﻿using System;

using DunkDunkers_RPG.Story;

namespace DunkDunkers_RPG
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello and welcome to DunkDunkers RPG! This is a short and simple game that tests a bit basic classes and unit testing in C# and Visual Studio." +
                " Enjoy :) ");
            Console.ReadKey();

            // Run Campaign 1   
            Campaign1.Run();
        }
    }
}
