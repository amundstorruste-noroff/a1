﻿using System;

namespace DunkDunkers_RPG.DataTypes
{
    // Enumeration used to assign Attribute used for damage calcualtion in a CombatHero object
    public enum PrimaryAttributeType { STRENGTH, DEXTERITY, INTELLIGENCE };

    /// <summary>
    /// Small data structure for storing integer values relating to PrimaryAttributes. 
    /// Chose struct to ensure new variable assignments were copies and not references, 
    /// which I felt made sense when overloding the + operator.
    /// </summary>
    public struct PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        /// <summary>
        /// Returns the value of a specific PrimaryAttributeType.
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns>The stored attribute value</returns>
        /// <exception cref="ArgumentException"></exception>
        public int GetAttributeValue(PrimaryAttributeType attribute)
        {
            if (attribute == PrimaryAttributeType.STRENGTH) return Strength;
            if (attribute == PrimaryAttributeType.DEXTERITY) return Dexterity;
            if (attribute == PrimaryAttributeType.INTELLIGENCE) return Intelligence;
            throw new ArgumentException("Could not find PrimaryAttributeType");
        }

        /// <summary>
        /// Addition with two PrimaryAttributes objects using the + operator creates a new PrimaryAttribtues object 
        /// with the sum of the properties. Simplifies leveling up and damage calculations.
        /// </summary>
        /// <param name="leftAttributes"></param>
        /// <param name="rightAttributes"></param>
        /// <returns></returns>
        public static PrimaryAttributes operator +(PrimaryAttributes leftAttributes, PrimaryAttributes rightAttributes)
            => new()
            {
                Strength = leftAttributes.Strength + rightAttributes.Strength,
                Dexterity = leftAttributes.Dexterity + rightAttributes.Dexterity,
                Intelligence = leftAttributes.Intelligence + rightAttributes.Intelligence,
            };

    }
}
