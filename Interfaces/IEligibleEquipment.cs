﻿using System.Collections.Generic;

namespace DunkDunkers_RPG.Interfaces
{
    /// <summary>
    /// Specifies lists for types of weapons and armor that are considered eligible to be equipped.
    /// </summary>
    public interface IEligibleEquipment
    {
        List<WeaponType> EligibleWeaponTypes { get; set; }
        List<ArmorType> EligibleArmorTypes { get; set; }

    }
}
