﻿using System;
using System.Drawing;
using System.Text;
using System.Threading;

using DunkDunkers_RPG.Character;
using DunkDunkers_RPG.Combat;
using DunkDunkers_RPG.Enemies;
using DunkDunkers_RPG.Enemies.EnemyTypes;
using DunkDunkers_RPG.Interfaces;

namespace DunkDunkers_RPG.Story
{
    public static class Campaign1
    {
        public static void Run()
        {
            Console.Clear();
            // Get name
            Console.WriteLine("Choose a name for your character:");
            string name = Console.ReadLine();

            // Get hero type
            Console.WriteLine("Chose your hero class: (Mage/Ranger/Rogue/Warrior)");
            string heroType = Console.ReadLine();
            CombatHero character = HeroFactory.CreateCombatHero(name, heroType);

            // Print hero stats
            Console.Clear();
            character.PrintStats();

            // Board the train
            Console.WriteLine("\npress any key to board the Train to Schmargonrog...");
            Console.ReadKey();

            // Lets go train choochoo
            Console.Clear();
            Console.WriteLine("The train departs to Schmargonrog and your journey begins...\n\n");
            Thread.Sleep(3000);

            // Train go BRRR
            ColorfulAnimation();

            // Combat 1
            var snugget = new TinyDunksnugget();
            BattleEnemy(character, snugget);

            // Level up
            Console.Clear();
            character.LevelUp();
            character.PrintStats();
            Console.ReadKey();

            // Reward 1
            if (character is IEligibleEquipment equipment) {

                WeaponType weaponType = equipment.EligibleWeaponTypes[0];
                string weaponTypeString = weaponType.ToString().ToLower();
                var weaponString = weaponTypeString.Substring(weaponTypeString.LastIndexOf('_') + 1);

                Weapon CommonWeapon = new()
                {
                    ItemName = $"Common {weaponString}",
                    ItemLevel = 1,
                    ItemSlot = Slot.SLOT_WEAPON,
                    WeaponType = weaponType,
                    WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
                };
                Console.WriteLine($"{character.Name} finds an item they can use!");
                Console.WriteLine($"Click <Enter> to equip {CommonWeapon.ItemName}.");
                if (Console.ReadKey().Key == ConsoleKey.Enter) character.EquipWeapon(CommonWeapon);
            }
            Console.Clear();
            character.PrintStats();
            Console.ReadKey();

            // Combat 2
            Console.Clear();
            var champion = new DunkmasterChampion();
            BattleEnemy(character, champion);

            // Level up 10 times
            for (int i = 0; i < 10; i++) character.LevelUp();
            Console.Clear();
            character.PrintStats();
            Console.ReadKey();

            // Reward 2
            if (character is IEligibleEquipment equipment1)
            {
                WeaponType weaponType = equipment1.EligibleWeaponTypes[0];
                string weaponTypeString = weaponType.ToString().ToLower();
                var weaponString = weaponTypeString.Substring(weaponTypeString.LastIndexOf('_') + 1);

                Weapon CommonWeapon = new()
                {
                    ItemName = $"Legendary {weaponString}",
                    ItemLevel = 10,
                    ItemSlot = Slot.SLOT_WEAPON,
                    WeaponType = weaponType,
                    WeaponAttributes = new WeaponAttributes() { Damage = 100, AttackSpeed = 2.5 }
                };

                Console.WriteLine($"{character.Name} finds an item they can use!");
                Console.WriteLine($"Click <Enter> to equip {CommonWeapon.ItemName}.");
                if (Console.ReadKey().Key == ConsoleKey.Enter) character.EquipWeapon(CommonWeapon);
            }
            Console.Clear();
            character.PrintStats();
            Console.ReadKey();

            // Combat 3
            var dragon = new DragonOfSchmargonrog();
            BattleEnemy(character, dragon);

            // Level up 100 times
            for (int i = 0; i < 987; i++) character.LevelUp();
            Console.Clear();
            character.PrintStats();
            Console.ReadKey();

            // Game complete
            Console.Clear();
            Console.WriteLine("And all was well..");
        }

        public static void BattleEnemy(CombatHero hero, Enemy enemy)
        {
            Console.Clear();
            Console.WriteLine($"{enemy.Name} attacks! Prepare for battle...");
            Console.WriteLine(enemy.AsciiArt);
            Console.ReadKey();

            int healthBase = enemy.Health;

            while (enemy.Health > 0)
            {
                // Draw new round of combat
                Console.Clear();
                Console.WriteLine();
                Console.WriteLine(enemy.AsciiArt);

                // Calcualte and draw new HP
                WriteHPbar(enemy.Health, healthBase);

                // Player move
                int damage = 0;
                Console.WriteLine("Click <Enter> to make an attack.");
                if(Console.ReadKey().Key == ConsoleKey.Enter)
                {
                    damage = (int)hero.CalculateDamage();
                    enemy.Health -= damage;
                }
                // Update
                Console.Clear();
                Console.WriteLine();
                Console.WriteLine(enemy.AsciiArt);
                WriteHPbar(enemy.Health, healthBase);
                Console.WriteLine($"{hero.Name} deals {damage} points of damage to {enemy.Name}");
                Console.ReadKey();

                // Enemy move (only if alive)
                if ( enemy.Health > 0 )
                {
                    Console.WriteLine();
                    enemy.Attack();
                    Console.ReadKey();
                }
            }
        }

        public static void WriteHPbar(int currentHealth, int healthBase)
        {
            double healthPercentage = (double)currentHealth / (double)healthBase * 100;
            string hpBar = MakeProgressBar(healthPercentage);
            Console.WriteLine($"{hpBar} HP: {currentHealth}/{healthBase}");
        }

        /// <summary>
        /// Writes a progress bar that displays one block ■ every 10 percent (rounded up)
        /// </summary>
        /// <param name="percent"></param>
        public static string MakeProgressBar(double percent)
        {
            StringBuilder progressBar = new StringBuilder();
            progressBar.Append('[');
            var numberOfSquares = Math.Ceiling((percent / 10d));
            for (var i = 0; i < 10; i++) {
                if (i < numberOfSquares) progressBar.Append('■');
                else progressBar.Append(' ');
            }
            progressBar.Append(']');
            return progressBar.ToString();
        }

        public static void ColorfulAnimation()
        {
            for (int i = 0; i < 1; i++)
            {
                for (int j = 0; j < 30; j++)
                {
                    Console.Clear();

                    // steam
                    Console.Write("       . . . . o o o o o o", Color.LightGray);
                    for (int s = 0; s < j / 2; s++)
                    {
                        Console.Write(" o", Color.LightGray);
                    }
                    Console.WriteLine();

                    var margin = "".PadLeft(j);
                    Console.WriteLine(margin + "                _____      o", Color.LightGray);
                    Console.WriteLine(margin + "       ____====  ]OO|_n_n__][.", Color.DeepSkyBlue);
                    Console.WriteLine(margin + "      [________]_|__|________)< ", Color.DeepSkyBlue);
                    Console.WriteLine(margin + "       oo    oo  'oo OOOO-| oo\\_", Color.Blue);
                    Console.WriteLine("   +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+", Color.Silver);

                    Thread.Sleep(100);
                }
            }
        }
    }
}
