﻿using DunkDunkers_RPG.Combat;
using DunkDunkers_RPG.DataTypes;
using DunkDunkers_RPG.Interfaces;
using System.Collections.Generic;

namespace DunkDunkers_RPG
{
    /// <summary>
    /// Strength based Combat Hero specializing in hand-to-hand combat and heavy armor.
    /// </summary>
    public class Warrior : CombatHero, IEligibleEquipment
    {
        public List<WeaponType> EligibleWeaponTypes { get; set; } = new() { WeaponType.WEAPON_AXE, WeaponType.WEAPON_HAMMER, WeaponType.WEAPON_SWORD };
        public List<ArmorType> EligibleArmorTypes { get; set; } = new() { ArmorType.ARMOR_MAIL, ArmorType.ARMOR_PLATE };

        public Warrior(string name) : base(name)
        {
            // Combat attribute used to calculate damage modifier
            DamageAttribute = PrimaryAttributeType.STRENGTH;

            // Set base stats
            BasePrimaryAttributes = new PrimaryAttributes { Strength = 5, Dexterity = 2, Intelligence = 1 };

            // Set lvl 1 stats
            TotalPrimaryAttributes = BasePrimaryAttributes;

            // Set stat increase on level up.
            StatsOnLevelUp = new PrimaryAttributes { Strength = 3, Dexterity = 2, Intelligence = 1 };
        }
    }
}
