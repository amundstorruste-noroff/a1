﻿using DunkDunkers_RPG.Combat;
using DunkDunkers_RPG.DataTypes;
using DunkDunkers_RPG.Interfaces;
using System.Collections.Generic;

namespace DunkDunkers_RPG
{
    /// <summary>
    /// Intelligence based Combat Hero specializing in magic weapons and light armor.
    /// </summary>
    public class Mage : CombatHero, IEligibleEquipment
    {
        public List<WeaponType> EligibleWeaponTypes { get; set; } = new() { WeaponType.WEAPON_STAFF, WeaponType.WEAPON_WAND };
        public List<ArmorType> EligibleArmorTypes { get; set; } = new() { ArmorType.ARMOR_CLOTH };

        public Mage(string name) : base(name)
        {
            // Combat attribute used to calculate damage modifier
            DamageAttribute = PrimaryAttributeType.INTELLIGENCE;

            // Set base stats
            BasePrimaryAttributes = new PrimaryAttributes { Strength = 1, Dexterity = 1, Intelligence = 8 };

            // Set lvl 1 stats
            TotalPrimaryAttributes = BasePrimaryAttributes;

            // Set stat increase on level up.
            StatsOnLevelUp = new PrimaryAttributes { Strength = 1, Dexterity = 1, Intelligence = 5 };
        }
    }
}
