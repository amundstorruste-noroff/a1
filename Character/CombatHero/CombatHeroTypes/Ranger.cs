﻿using DunkDunkers_RPG.Combat;
using DunkDunkers_RPG.DataTypes;
using DunkDunkers_RPG.Interfaces;
using System.Collections.Generic;

namespace DunkDunkers_RPG
{
    /// <summary>
    /// Dexterity based based Combat Hero specializing in ranged combat and medium armor.
    /// </summary>
    public class Ranger : CombatHero, IEligibleEquipment
    {
        public List<WeaponType> EligibleWeaponTypes { get; set; } = new() { WeaponType.WEAPON_BOW  };
        public List<ArmorType> EligibleArmorTypes { get; set; } = new() { ArmorType.ARMOR_LEATHER, ArmorType.ARMOR_MAIL };

        public Ranger(string name) : base(name)
        {
            // Combat attribute used to calculate damage modifier
            DamageAttribute = PrimaryAttributeType.DEXTERITY;

            // Set base stats
            BasePrimaryAttributes = new PrimaryAttributes { Strength = 1, Dexterity = 7, Intelligence = 1 };

            // Set lvl 1 stats
            TotalPrimaryAttributes = BasePrimaryAttributes;

            // Set stat increase on level up.
            StatsOnLevelUp = new PrimaryAttributes { Strength = 1, Dexterity = 5, Intelligence = 1 };
        }
    }
}
