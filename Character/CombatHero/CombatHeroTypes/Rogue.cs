﻿using DunkDunkers_RPG.Combat;
using DunkDunkers_RPG.DataTypes;
using DunkDunkers_RPG.Interfaces;
using System.Collections.Generic;

namespace DunkDunkers_RPG
{
    /// <summary>
    /// Dexterity based based Combat Hero specializing in hand-to-hand combat and medium armor.
    /// </summary>
    public class Rogue : CombatHero, IEligibleEquipment
    {
        public List<WeaponType> EligibleWeaponTypes { get; set; } = new() { WeaponType.WEAPON_DAGGER, WeaponType.WEAPON_SWORD };
        public List<ArmorType> EligibleArmorTypes { get; set; } = new() { ArmorType.ARMOR_LEATHER, ArmorType.ARMOR_MAIL };

        public Rogue(string name) : base(name)
        {
            // Combat attribute used to calculate damage modifier
            DamageAttribute = PrimaryAttributeType.DEXTERITY;

            // Set base stats
            BasePrimaryAttributes = new PrimaryAttributes { Strength = 2, Dexterity = 6, Intelligence = 1 };

            // Set lvl 1 stats
            TotalPrimaryAttributes = BasePrimaryAttributes;

            // Set stat increase on level up.
            StatsOnLevelUp = new PrimaryAttributes { Strength = 1, Dexterity = 4, Intelligence = 1 };
        }
    }
}
