﻿using DunkDunkers_RPG.DataTypes;
using DunkDunkers_RPG.Character;
using System;
using System.Collections.Generic;
using System.Text;
using DunkDunkers_RPG.Interfaces;
using DunkDunkers_RPG.CustomExceptions;

namespace DunkDunkers_RPG.Combat
{
    /// <summary>
    /// Combat Heroes can fight and their Calculate Damage dealt based on their Inventory equipment and Primary Attributes.
    /// </summary>
    public abstract class CombatHero : Hero
    {
        // Combat attribute used to calculate damage modifier
        public PrimaryAttributeType DamageAttribute { get; init; }

        public Inventory Inventory { get; init; } = new();

        // Constructor
        protected CombatHero(string name) : base(name){}

        // Calculate damage dealt based on equipped weapon (if any), Hero attributes, and Armor attribute bonuses.
        public double CalculateDamage()
        {
            // Check weapon and armor in inventory
            Weapon weapon = Inventory.GetWeapon();
            List<Armor> armorList = Inventory.GetArmorList();

            // Calculate total combined attributes from armor and hero stats
            PrimaryAttributes totalCombinedAttributes = GetTotalCombinedAttributes(armorList);

            // Calculate modifiers
            double weaponDPS = (weapon is not null) ? weapon.GetDPS() : 1;
            double damageModifier = totalCombinedAttributes.GetAttributeValue(DamageAttribute);

            return CombatMoves.MeleeWeaponAttack(weaponDPS, damageModifier);
        }

        /// <summary>
        /// Equips Weapon into the Inventory of a CombatHero if eligibility and level requirements are met.
        /// </summary>
        /// <param name="weapon"></param>
        /// <exception cref="InvalidWeaponException"></exception>
        public void EquipWeapon(Weapon weapon)
        {
            // Check if equipment eligibilty restrictions exist
            if (this is IEligibleEquipment check)
            {
                if (check.EligibleWeaponTypes.Contains(weapon.WeaponType) == false)
                    throw new InvalidWeaponException("Invalid weapon type.");
            }
            if (Level < weapon.ItemLevel)
                throw new InvalidWeaponException("Too low level.");

            // Add armor to inventory and display success message
            Inventory.EquipWeapon(weapon);
        }

        /// <summary>
        /// Equips Armor into the Inventory of a CombatHero if eligibility and level requirements are met.
        /// </summary>
        /// <param name="armor"></param>
        /// <exception cref="InvalidArmorException"></exception>
        public void EquipArmor(Armor armor) 
        {
            // Check if equipment eligibilty restrictions exist
            if (this is IEligibleEquipment check)
                if (check.EligibleArmorTypes.Contains(armor.ArmorType) == false)
                    throw new InvalidArmorException("Invalid armor type.");

            if (Level < armor.ItemLevel) throw new InvalidArmorException("Too low level.");

            // Add armor to inventory and display success message
            Inventory.EquipArmor(armor);
        }

        /// <summary>
        /// Prints the stats of the Hero based on their level and equipment.
        /// </summary>
        public void PrintStats()
        {
            // Calculate total combined attributes from armor and hero stats
            List<Armor> armorList = Inventory.GetArmorList();
            PrimaryAttributes totalCombinedAttributes = GetTotalCombinedAttributes(armorList);
     
            // Write to console
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"{Name} the ultimate {this.GetType().Name}");
            sb.AppendLine($"Level: {Level}");
            sb.AppendLine($"Strength: {totalCombinedAttributes.Strength}");
            sb.AppendLine($"Dexterity: {totalCombinedAttributes.Dexterity}");
            sb.AppendLine($"Intelligence: {totalCombinedAttributes.Intelligence}");
            sb.AppendLine($"Damage: {CalculateDamage()}");
            Console.WriteLine(sb);
        }

        /// <summary>
        /// Returns the total PrimaryAttributes from Armor in the Inventory of a CombatHero. 
        /// </summary>
        /// <param name="armorList"></param>
        /// <returns></returns>
        private PrimaryAttributes GetTotalCombinedAttributes(List<Armor> armorList)
        {
            PrimaryAttributes armorAttributes = new PrimaryAttributes { Strength = 0, Dexterity = 0, Intelligence = 0};
            foreach (Armor armor in armorList) armorAttributes += armor.Attributes;

            // Calculate total combined attributes from armor and hero stats
            PrimaryAttributes totalCombinedAttributes = armorAttributes + TotalPrimaryAttributes;

            return totalCombinedAttributes;
        }
    }
}
