﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DunkDunkers_RPG.Combat
{
    /// <summary>
    /// Contains methods for calculating damage from different types of attacks.
    /// </summary>
    public static class CombatMoves
    {
        /// <summary>
        /// Basic attack mean to be used by all CombatHeroTypes.
        /// </summary>
        /// <param name="weaponDPS"></param>
        /// <param name="damageModifier"></param>
        /// <returns>Calculated damage</returns>
        public static double MeleeWeaponAttack(double weaponDPS, double damageModifier)
        {
            return weaponDPS * (1 + damageModifier / 100);
        }
    }

}
