﻿using DunkDunkers_RPG.Combat;

namespace DunkDunkers_RPG.Character
{
    /// <summary>
    /// Specialized object solely for creating other objects. Utilizes the Factory Design Pattern.
    /// </summary>
    public static class HeroFactory
    {
        public static CombatHero CreateCombatHero(string heroName, string heroType)
        {
            if (heroType == "Mage") return new Mage(heroName);
            if (heroType == "Ranger") return new Ranger(heroName);
            if (heroType == "Rogue") return new Rogue(heroName);
            if (heroType == "Warrior") return new Warrior(heroName);
            return null;
        }
    }
}
