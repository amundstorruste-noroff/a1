﻿using DunkDunkers_RPG.DataTypes;

namespace DunkDunkers_RPG.Character
{
    /// <summary>
    /// Main Character class for the DunkDunkers RPG application. Specializes into one of four Combat Hero Types.
    /// </summary>
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }
        public PrimaryAttributes StatsOnLevelUp { get; set; }

        // Constructor
        public Hero(string name)
        {
            Name = name;
            Level = 1;
        }

        /// <summary>
        /// Leveling up increases the primary attributes of a Hero.
        /// </summary>
        public void LevelUp()
        {
            Level++;
            TotalPrimaryAttributes += StatsOnLevelUp;
        }
    }
}
